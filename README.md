# Useful commands

```
rsync -ah --info=progress2

dd if=input of=output status=progress && sync

grep -irn

ls -Alh

dpkg --configure -a

dpkg -l package* | grep ^i

find . -type f -name '*.JPG' | while read f; do mv -vf "$f" "${f%.*}.jpg"; done #https://www.unix.com/unix-for-dummies-questions-and-answers/261342-find-rename-file-recursively.html

df -h /

ncdu /var

telnet monserveur.net 443

ping -p 22 monserveur.com

inginx -c /etc/nginx/nginx.conf -t #Vérif config Nginx

netstat -nlp

for i in *.jpg; do convmv -f cp437 -t utf-8 -r --nosmart --notest "$i"; done #Convert files names from cp437 to utf-8 encoding using convmv.

iconv -f iso8859-1 -t utf8 filename #Convert files encoding from cp437 to utf-8.

pandoc -s source-file.md -o target-file.odt

lastb #List the last connected users

```
